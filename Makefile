COMPILE=latexmk
VIEWER=evince

RM=rm -rf

FLAGS=-pdf -shell-escape

SRC_FR=main_fr.tex tpcs.sty tpcs_fr.sty $(wildcard src_fr/*.tex) $(wildcard src_fr/**/*.tex) $(wildcard assets/*)
SRC_EN=main_en.tex tpcs.sty tpcs_en.sty $(wildcard src_en/*.tex) $(wildcard src_en/**/*.tex) $(wildcard assets/*)

OUT_FR=tp-introduction-fr
OUT_EN=tp-introduction-en

all: $(OUT_FR).pdf $(OUT_EN).pdf

$(OUT_FR).pdf: $(SRC_FR)
	$(COMPILE) $(FLAGS) -jobname=$(OUT_FR) $<

$(OUT_EN).pdf: $(SRC_EN)
	$(COMPILE) $(FLAGS) -jobname=$(OUT_EN) $<

view: $(OUT_FR).pdf $(OUT_EN).pdf
	$(VIEWER) $(OUT_FR).pdf $(OUT_EN).pdf

clean:
	$(RM) *.aux *.log *.nav *.toc *.snm *.out *.pyg _minted* *.fls *.fdb_latexmk

distclean: $(clean)
	$(RM) $(OUT_FR).pdf $(OUT_EN).pdf

.PHONY: view clean
