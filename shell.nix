{ pkgs ? import <nixpkgs> { } }:

pkgs.mkShell {
  buildInputs = with pkgs; [
    (texlive.combine {
      inherit (pkgs.texlive) scheme-full pygmentex;
    })
    pygmentex
    python27Packages.pygments
    gnumake
    evince
  ];
}
