\subsection{i3}

Any desktop environment uses a \emph{window manager} to position windows on the
screen. On the PIE, the available window manager is named \textbf{i3}. It is a
\emph{tiling window manager}, meaning that it positions windows on its own,
aiming to avoid having them overlapping one another.

The main advantage of a \emph{tiling window manager} is to free the user from
the tedious task of managing the windows (e.g. moving them around, resizing
them\ldots), which makes you work more efficiently. It is especially useful for
programming, since that requires having both source code and documentation on
your screen at any given moment.

On top of that, what little window management we need to do ourselves can be
done with keyboard shortcuts. You will find a small cheat sheet with the most
important shortcuts below. The \keystroke{Mod} key corresponds to \Alt or
\keystroke{Windows} depending on the choice you made when you first logged in.

\begin{tip}{Fundamentals}
  \begin{description}
    \item [\keystroke{Mod}+\Shift+\keystroke{E}] Exit i3 and go back to the log
      in screen.
    \item [\keystroke{Mod}+\Enter] Open a terminal
    \item [\keystroke{Mod}+\Shift+\keystroke{Q}] Close a window
    \item [\keystroke{Mod}+\Shift+\LArrow\DArrow\UArrow\RArrow] Move a window
    \item [\keystroke{Mod}+\keystroke{d}] Launch \texttt{dmenu}, an application
      launcher
  \end{description}
\end{tip}

\begin{exercise}{Exercise}
  Open multiple terminals using \keystroke{Mod}+\Enter and observe how each
  window take up the available screen space.
\end{exercise}

Test each of the commands seen above. Commands that affect a window will do so
on the currently active one. By default, this is the window under your mouse
cursor. The active window has a blue frame and inactive windows have a black
frame. \texttt{dmenu} will show up on top of the screen and allows you to type
in \texttt{"firefox"} and press \Enter to launch the browser.

\begin{gf}{To go further}
  i3 can put windows on different "virtual desktops". Each desktop is numbered
  from 1 to 10 by default. You can, for example, move to the second desktop by
  using the \keystroke{Mod}+\keystroke{2}\footnote{You must use the number keys
  that are on the keyboard's top row. The numeric pad keys (the cluster on the
  right side of the keyboard) may not work.} shortcut.\footnote{The 10th
  desktop corresponds to the keyboard's \keystroke{0} key}

  It is also possible to move windows from one desktop to another. For example,
  while having a window focused in the 1st desktop, you can move it to the 2nd
  desktop by using the \keystroke{Mod}+\Shift+\keystroke{2} shortcut.

  Currently used virtual desktops will show up on the bottom left corner of
  your screen.

  \begin{description}
    \item [i3 documentation] \url{https://i3wm.org/docs/userguide.html}
    \item [i3 reference card] \url{https://i3wm.org/docs/refcard.html}
  \end{description}
\end{gf}

\subsubsection{i3lock}

  Sometimes, you may need to leave the computer you are using unattended for
  some time. Should you leave your session freely available to anyone, you are
  giving anyone passing by access to all of your accounts and sensitive
  information. You are also at the mercy of the \emph{confloose}, a series of
  nasty configurations made to maximize discomfort and minimize how easy
  recovering from such an attack would be. More importantly, you are
  responsible for any of the potentially illegal acts committed from your
  session.

  To avoid having to log out then log back in every single time you leave your
  computer, you can use a \emph{screen locker}. The \emph{screen locker}
  available on the PIE is named \texttt{i3lock}.

  Once \texttt{i3lock} is launched, you will have to type in your password
  followed by \Enter to unlock your session.

  \begin{tip}{Fundamentals}
    Launch \texttt{i3lock} using \texttt{dmenu}, then unlock your session.
  \end{tip}

  \begin{important}{Important}
    It is forbidden to leave a session locked for more than an hour at EPITA.
  \end{important}
