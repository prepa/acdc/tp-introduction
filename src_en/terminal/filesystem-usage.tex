\subsection{Moving in a filesystem}

First step, finding your way around: the \mintinline{bash}{pwd}\footnote{Stands 
for \emph{print working directory}} command prints the name of the directory 
in which you currently are.

\begin{minted}{shell-session}
  $ pwd
  /home/firstname.name
\end{minted}

\begin{exercise}{Exercises}
  Enter the \mintinline{bash}{pwd} command and observe the output.
\end{exercise}

In fact, \mintinline{bash}{pwd} displays the absolute path of the current
directory, that is to say the complete path starting from the root. As a
reminder, the root is the highest possible level directory, which contains all
the others.

If a path does not start with a \mintinline{bash}{/}, this is a path that
starts from the current directory, that is to say a relative path.

\begin{tip}{Fundamentals}
  The next step is to move in the tree: we are going to change the current
  directory. The command that execute this action is
  \mintinline{bash}{cd}\footnote{Stands for \emph{change directory}}.  The
  \mintinline{bash}{cd} command requires an argument: the path (relative or
  absolute) of the directory that you want to move in.

  The general syntax to pass an argument to a program is the following:

  \begin{minted}{bash}
    name-of-the-program argument1 argument2 argument3
  \end{minted}
\end{tip}


\begin{gf}{To go further}
  The \mintinline{bash}{cd} command can also be used without arguments to go
  back to the home directory
\end{gf}

\subsubsection{Some examples}

  \begin{minted}{shell-session}
    $ cd /
    $ pwd
    /
    $ cd /home
    $ pwd
    /home
  \end{minted}

\begin{exercise}{Exercises}
  Enter the command \mintinline{bash}{cd /home}, then the command
  \mintinline{bash}{pwd}, observe the output and then go back to your home
  directory
\end{exercise}

\begin{gf}{Home sweet home}
  The shortcut \mintinline{bash}{~} allows you to indicate your home directory
  without having to type \mintinline{bash}{/home/firstname.name}.
\end{gf}

The \mintinline{bash}{ls} command lists the contents of a directory.

By default, \mintinline{bash}{ls} lists the contents of the current directory.
The \mintinline{bash}{ls} command can also be used with arguments in order to
list the contents of other directories.

\begin{minted}{shell-session}
  $ ls /
  bin  boot  dev  etc  home  lib  lib64 …
  $ ls /home
  firstname.name
\end{minted}

These commands offer features close to graphical file explorers.

The commands like \mintinline{bash}{ls} can be used with options (which often
start with \mintinline{bash}{-}). For example, the option \mintinline{bash}{-a}
tells the command to also list the hidden files (when using Linux, a hidden file has
a "\mintinline{bash}{.}" at the beginning of its name)

\begin{minted}{shell-session}
  $ cd
  $ ls
  afs
  $ ls -a
  . .. afs .bash_history .bashrc
\end{minted}

\begin{exercise}{Exercises}
  Enter the command \mintinline{bash}{ls -a} and observe the output. What is
  the contents of the directory \texttt{.config} in your home directory ?
\end{exercise}

\subsubsection{Create and modify resources}
  The command \mintinline{bash}{mkdir}\footnote{\gcommand{m}a\gcommand{k}e
  \gcommand{dir}ectory} creates a directory.

  \begin{minted}{shell-session}
    $ cd
    $ mkdir test_directory
    $ ls
    afs test_directory
  \end{minted}

  \begin{exercise}{Exercises}
    Create a directory named \texttt{directory\_test} in your home directory.
  \end{exercise}

  You can use the command \texttt{gedit} to launch a graphic text editor. You
  can pass as an argument the path to the file you want to edit.

  \begin{minted}{shell-session}
    $ gedit my-file
    * a wild editor appears *
  \end{minted}

  The \mintinline{bash}{cat} command displays the content of a file

  \begin{exercise}{Exercises}
    Create a file and print its content in a terminal.
  \end{exercise}

  \begin{minted}{shell-session}
    $ cat my-file
    some text written in gedit !
  \end{minted}

  Lots of other commands are available to manipulate files. For example, the
  \mintinline{bash}{mv} command can move files and
  directories\footnote{\gcommand{m}o\gcommand{v}e}.

  The first argument of \gcommand{mv} is the current path of the file or
  directory to move, and the second argument of \gcommand{mv} is the new path
  of the file, \textbf{or} the path of the directory in which the file will be
  moved to.

  \begin{minted}{shell-session}
    $ mv my_file other_file
    * Rename my_file en other_file *
    $ mv other_file directory_test/yet_another_one
    * Rename other_file in yet_another_one, et put it in the directory directory_test *
    $ mv directory_test/yet_another_one ./
    * Move without renaming directory_test/yet_another_one to the current directory *
    $ ls
    afs yet_another_one directory_test
  \end{minted}

  \begin{exercise}{Exercises}
    The command to rename a file is the same as the one to move a file. Indeed,
    renaming is the same as moving a file to the same directory with a
    different name.

    Rename one of your files, then move it.
  \end{exercise}

  \begin{important}{Important}
    If you move a file by indicating the path to a file that already exists,
    the destination file will be replaced by the source file without asking for
    any confirmation!
  \end{important}

  The command \gcommand{cp}\footnote{\gcommand{c}o\gcommand{p}y}
  duplicates a file.

  \begin{minted}{shell-session}
    $ cat yet_another_one
    some text written in gedit !
    $ cp yet_another_one heyhey
    $ ls
    afs yet_another_one heyhey directory_test
    $ cat heyhey
    some text written in gedit !
  \end{minted}

  \begin{exercise}{Exercises}
    Copy one of your files.
  \end{exercise}

  The command \mintinline{bash}{rm}
  removes\footnote{\gcommand{r}e\gcommand{m}ove} a file.

  \begin{important}{Attention}
    Be careful, or you risk to lose importants files: unlike Windows, there is
    no bin to recover deleted files.
  \end{important}

  \begin{minted}{shell-session}
    $ ls
    afs yet_another_one heyhey directory_test
    $ rm heyhey
    $ ls
    afs yet_another_one directory_test
  \end{minted}

  \begin{exercise}{Exercises}
    Copy a file, print its contents and delete it.
  \end{exercise}
