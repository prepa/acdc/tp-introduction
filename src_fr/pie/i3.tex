\subsection{i3}

Tout environnement de bureau fait appel à un \emph{gestionnaire de fenêtres} ou
(\emph{window manager}) pour positionner les fenêtres sur l'écran. Sur le PIE,
le gestionnaire de fenêtres disponible se nomme \textbf{i3}. Il a la
particularité d'être un \emph{tiling window manager}, c'est-à-dire qu'il
positionne lui-même les fenêtres de façon à ce qu'elles ne se recouvrent pas.

L'intérêt d'un \emph{tiling window manager} est de libérer l'utilisateur d'une
fastidieuse gestion des fenêtres et donc de gagner en efficacité. C'est tout
particulièrement adapté à la programmation, qui est une activité qui nécessite
d'avoir du code source et de la documentation sous les yeux en permanence.

Cerise sur le gâteau, ce qui reste de gestion des fenêtres peut s'effectuer à
l'aide de raccourcis clavier. Ci-dessous figure un petit aide mémoire des
raccourcis usuels. La touche \keystroke{Mod} correspond à \Alt ou
\keystroke{Windows} en fonction du choix que vous avez fait lors du premier
démarrage de votre session. Ce choix peut être modifié dans le fichier de
configuration d'i3.

\begin{tip}{Fondamentaux}
  \begin{description}
    \item [\keystroke{Mod}+\Shift+\keystroke{E}] Quitter i3 et revenir à
      l'écran de connexion (après confirmation)
    \item [\keystroke{Mod}+\Enter] Ouvrir un terminal
    \item [\keystroke{Mod}+\Shift+\keystroke{Q}] Fermer une fênetre
    \item [\keystroke{Mod}+\Shift+\LArrow\DArrow\UArrow\RArrow] Déplacer une
      fenêtre
    \item [\keystroke{Mod}+\keystroke{d}] Lancer \texttt{dmenu}, un lanceur
      d'application
  \end{description}
\end{tip}

\begin{exercise}{Exercice}
  Ouvrez plusieurs terminaux à l'aide de \keystroke{Mod}+\Enter et observez
  comment les fenêtres se répartissent la surface d'écran disponible.
\end{exercise}

Testez une à une les commandes exposées ci-dessus, les commandes qui
s'appliquent à une fenêtre se font sur celle qui est active. Par défaut, il
s'agit de la fenêtre en dessous de la souris. Cette fenêtre est encadrée en
bleu clair, contrairement aux inactives encadrées en noir. \texttt{dmenu}
s'affichera en haut de l'écran, vous pourrez alors taper
\og~\texttt{firefox}~\fg puis \Enter pour lancer un navigateur.

\begin{gf}{Pour aller plus loin}
  i3 offre la possibilité de répartir les fenêtres sur des bureaux virtuels.
  Les bureaux virtuels sont numérotés de 1 à 10 par défaut. Vous pouvez vous
  déplacer sur le bureau 2, par exemple, en utilisant le raccourci
  \keystroke{Mod}+\keystroke{2}\footnote{Il faut utiliser les touches
  numériques de la rangée du haut et non pas celles du pavé
  numérique.}\footnote{Le bureau numéroté 10 correspond à la touche
  \keystroke{0} du clavier}.

  Il est également possible de déplacer les fenêtres d'un bureau à l'autre. En
  ayant le focus sur une fenêtre du bureau 1 que vous voulez déplacer sur le
  bureau 2, utilisez le raccourci \keystroke{Mod}+\Shift+\keystroke{2}.

  Les bureaux virtuels en cours d'utilisation s'affichent en bas à gauche de
  l'écran.

  \begin{description}
    \item [Documentation d'i3] \url{https://i3wm.org/docs/userguide.html}
    \item [Référence i3] \url{https://i3wm.org/docs/refcard.html}
  \end{description}
\end{gf}

\subsubsection{i3lock}

  Il peut arriver que vous ayez besoin de vous absenter un court instant de
  votre poste alors que vous êtes en train de travailler dessus. Si vous
  laissez votre session accessible à n'importe qui, vous laissez accès à cette
  personne à tous vos comptes, vos informations personnelles et
  confidentielles. Vous vous mettez également à la merci d'une
  \emph{confloose}, c'est-à-dire à une série de configurations spécifiquement
  pensées pour maximiser l'inconfort dans l'utilisation de votre machine. Plus
  grave, vous vous exposez aussi à endosser la responsabilité des potentiels
  actes illégaux commis depuis votre session.

  Pour éviter d'avoir à vous déconnecter puis reconnecter chaque fois que vous
  quittez votre poste, vous pouvez utiliser un \emph{screen locker}. Le
  \emph{screen locker} disponible sur le PIE se nomme \texttt{i3lock}.

  Une fois \texttt{i3lock} enclenché, vous devrez taper votre mot de passe
  suivi de \Enter pour déverrouiller votre session.

  \begin{tip}{Fondamentaux}
    Lancez \texttt{i3lock} à l'aide de \texttt{dmenu}, puis déverrouillez votre
    session.
  \end{tip}

  \begin{important}{Attention}
    À EPITA, il est interdit de laisser une session verrouillée plus d'une
    heure.
  \end{important}
