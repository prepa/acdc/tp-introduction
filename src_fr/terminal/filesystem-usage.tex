\subsection{Se déplacer dans le système de fichiers}

Première étape, s'orienter~: la commande \mintinline{bash}{pwd}\footnote{Pour
\emph{print working directory}} permet d'afficher le nom du dossier dans lequel
vous vous trouvez actuellement.

\begin{minted}{shell-session}
  $ pwd
  /home/prénom.nom
\end{minted}

\begin{exercise}{Exercice}
  Entrez la commande \mintinline{bash}{pwd} et observez le résultat.
\end{exercise}

En fait, \mintinline{bash}{pwd} affiche le chemin absolu du dossier actuel,
c'est-à-dire le chemin complet depuis la racine. Pour rappel, la racine est le
dossier de plus haut niveau, qui contient tous les autres.

Si un chemin ne commence pas par un \mintinline{bash}{/}, c'est un chemin qui
débute depuis le dossier actuel, un chemin relatif donc.

\begin{tip}{Fondamentaux}
  La prochaine étape va être de se déplacer dans l'arborescence~: nous allons
  changer le dossier courant. La commande qui permet d'effectuer cette action
  se nomme \mintinline{bash}{cd}\footnote{Pour \emph{change directory}}.

  La commande \mintinline{bash}{cd} requiert un argument, le chemin (relatif ou
  absolu) du dossier dans lequel se déplacer.

  La syntaxe générale pour passer un argument à un programme est la suivante~:

  \mintinline{bash}{nom-du-programme argument1 argument2 argument3}
\end{tip}

\begin{gf}{Pour aller plus loin}
  La commande \mintinline{bash}{cd} peut également être utilisée sans arguments
  pour revenir dans votre répertoire utilisateur.
\end{gf}

\subsubsection{Quelques exemples}

  \begin{minted}{shell-session}
    $ cd /
    $ pwd
    /
    $ cd /home
    $ pwd
    /home
  \end{minted}

\begin{exercise}{Exercice}
  Entrez la commande \mintinline{bash}{cd /home}, puis la commande
  \mintinline{bash}{pwd}, observez le résultat puis retournez dans votre
  dossier personnel.
\end{exercise}

\begin{gf}{Home sweet home}
  Le raccourci \mintinline{bash}{~} vous permet de faire référence à votre
  dossier personnel sans avoir à taper \mintinline{bash}{/home/prénom.nom}.
\end{gf}

La commande \mintinline{bash}{ls} permet de lister le contenu d'un dossier.

Par défaut, \mintinline{bash}{ls} liste le contenu du dossier courant. La
commande \mintinline{bash}{ls} peut également être utilisée avec des arguments
pour lister le contenu d'autres dossiers.

\begin{minted}{shell-session}
  $ ls /
  bin  boot  dev  etc  home  lib  lib64 …
  $ ls /home
  prénom.nom
\end{minted}

Ces commandes offrent des fonctionnalités proches d'explorateurs de fichiers
graphiques.

Les commandes comme \mintinline{bash}{ls} peuvent être utilisées avec des
options (qui commencent souvent par \mintinline{bash}{-}). Par exemple,
l'option \mintinline{bash}{-a} permet d'afficher les fichiers cachés (sous
Linux, un fichier caché est un fichier commençant par un
\og\mintinline{bash}{.}\fg).

\begin{minted}{shell-session}
  $ cd
  $ ls
  afs
  $ ls -a
  . .. afs .bash_history .bashrc
\end{minted}

\begin{exercise}{Exercice}
  Entrez la commande \mintinline{bash}{ls -a} et observez le résultat.  Que
  contient le dossier \texttt{.config}, dans votre espace personnel~?

  Entrez la commande \mintinline{bash}{ls -l} et observez le résultat.
\end{exercise}

\subsubsection{Créer et modifier des ressources}

  La commande \mintinline{bash}{mkdir}\footnote{\gcommand{m}a\gcommand{k}e
  \gcommand{dir}ectory} permet de créer un dossier.

  \begin{minted}{shell-session}
    $ cd
    $ mkdir dossier_test
    $ ls
    afs dossier_test
  \end{minted}

  \begin{exercise}{Exercice}
    Créez un dossier nommé \texttt{dossier\_test} dans votre espace personnel.
  \end{exercise}

  Vous pouvez utiliser la commande \texttt{gedit} pour lancer un éditeur de
  texte graphique.  Vous pouvez donner en argument le chemin du fichier à
  éditer.

  \begin{minted}{shell-session}
    $ gedit mon_fichier
    * un éditeur apparaît *
  \end{minted}

  La commande \mintinline{bash}{cat} permet d'afficher le contenu d'un fichier.

  \begin{exercise}{Exercice}
    Créez un fichier puis affichez son contenu dans un terminal.
  \end{exercise}

  \begin{minted}{shell-session}
    $ cat mon_fichier
    du texte tapé dans gedit !
    \end{minted}

  Beaucoup d'autres commandes sont disponibles pour manipuler les fichiers. Par
  exemple \mintinline{bash}{mv} permet de déplacer des fichiers ainsi que des
  dossiers \footnote{\gcommand{m}o\gcommand{v}e en anglais}.

  Le premier argument de \gcommand{mv} est le chemin actuel du fichier ou
  dossier à déplacer, et le second argument de \gcommand{mv} est le nouveau
  chemin du fichier, \textbf{ou} le chemin du dossier dans lequel le déplacer.

  \begin{minted}{shell-session}
    $ mv mon_fichier autre_fichier
    * Renomme mon_fichier en autre_fichier *
    $ mv autre_fichier dossier_test/encore_un_autre
    * Renomme autre_fichier en encore_un_autre, et le met dans le dossier dossier_test *
    $ mv dossier_test/encore_un_autre ./
    * Bouge sans renommer encore_un_autre dans le dossier actuel *
    $ ls
    afs encore_un_autre dossier_test
  \end{minted}

  \begin{exercise}{Exercice}
    La commande pour renommer un fichier est la même que pour déplacer un
    fichier, renommer revient effectivement à déplacer un fichier dans le
    même dossier avec un nom différent.

    Renommez un de vos fichiers, puis déplacez-le.
  \end{exercise}

  \begin{important}{Attention}
    Si vous déplacez un fichier en indiquant le chemin d'un autre fichier qui
    existe déjà, le fichier de destination sera remplacé par le fichier source
    sans demander de confirmation !
  \end{important}

  La commande \gcommand{cp}\footnote{\gcommand{c}o\gcommand{p}y, en anglais}
  permet de dupliquer un fichier.

  \begin{minted}{shell-session}
    $ cat encore_un_autre
    du texte tapé dans gedit !
    $ cp encore_un_autre heyhey
    $ ls
    afs encore_un_autre heyhey dossier_test
    $ cat heyhey
    du texte tapé dans gedit !
  \end{minted}

  \begin{exercise}{Exercice}
    Copiez un de vos fichiers.
  \end{exercise}

  La commande \mintinline{bash}{rm} permet de
  supprimer\footnote{\gcommand{r}e\gcommand{m}ove, en anglais} un fichier.

  \begin{important}{Attention}
    Soyez prudent{\pointmedian}e{\pointmedian}s, ou vous risquez de perdre des
    fichiers importants~: contrairement à Windows, il n'y a pas de corbeille où
    récupérer les fichiers supprimés.
  \end{important}

  \begin{minted}{shell-session}
    $ ls
    afs encore_un_autre heyhey dossier_test
    $ rm heyhey
    $ ls
    afs encore_un_autre dossier_test
  \end{minted}

  \begin{exercise}{Exercice}
    Copiez un fichier, affichez son contenu puis effacez le.
  \end{exercise}
